﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample_3_SOLID.Content
{
    public class InterfaceSegregationPrinciple
    {
        // Arayüzlerin Ayrımı Prensibi (ISP: Interface Segregation Principle)
        // Slogan : "Nesneler, ihtiyaç duymadıkları metotların bulunduğu Interface’lere bağlı olmaya zorlanmamalıdır".
        // Seneryo :  Senaryomuz, bir sosyal mesajlaşma platformu olsun.
        //            Bu platformun üyeleri birbirlerine sesli, görüntülü veya sadece metin mesajı gönderebilirler. 
        //            Sizde mimari açıdan bir interface tasarlamanın uygun olduğuna karar verdiniz.
        // Çözüm : IMessage interface’inin birçok amaca hizmet ettiğini görebiliyoruz. Şimdi tek yapmamız gereken, bu amaçları birbirinden ayırmak.
        //         Tüm mesajlaşmalar From, To ve MessageBody özelliklerini içermek zorunda.
        //         Resim içeren mesajlar, AttachImage metodunu ve ImageWidth, ImageHeight özelliklerini ve taşımak zorunda.
        //         Video içeren mesajlar, AttachMovie metodunu ve MovieDuration özelliğini içermek zorunda.

        #region Mesaj Örneği
        public interface IMessage
        {
            string From { get; set; }
            string MessageBody { get; set; }
            string To { get; set; }
        }

        public interface IMessageImage : IMessage
        {
            int ImageWitdh { get; set; }
            int ImageHeight { get; set; }
            void AttachImage();
        }

        public interface IMovieMessage : IMessage
        {
            int MovieDuration { get; set; }
            void AttachMovie();
        }
        #endregion
    }
}
