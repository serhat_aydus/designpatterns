﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample_3_SOLID.Content
{
    public class DependencyInversionPrinciple
    {
        // Bağlılığı Tersine Çevirme Prensibi (Dependency Inversion Principle – DIP)
        // Slogan : "Üst seviyeli sınıfın alt seveli sınıftan Interface aracılığı ile bağımlılığını ortadan kaldırmaktır".
        // Seneryo : Evinizdeki ampul patladığında koskoca elektrik tesisatını değiştirdiğinizi bir düşünsenize! 
        // Yani büyük modül (elektrik tesisatı) küçük modüle (ampul) bağlı olmamalı! Her ikisi de soyut kavrama (lamba –duy ve ampul-) bağlı olmalı. 
        // Üstelik burada, ampulün kaç Watt olduğu (detay), lambanın duy kısmını (soyut) ilgilendirir mi ? isterseniz florasan takın. 
    }

    #region Converter Örneği
    public interface IDataConverter
    {
        void convert(List<Product> productList);
    }

    public class XmlConverter : IDataConverter
    {
        public void convert(List<Product> productList)
        {
            throw new NotImplementedException();
        }
    }

    public class JsonConverter : IDataConverter
    {
        public void convert(List<Product> productList)
        {
            throw new NotImplementedException();
        }
    }
    public class Product
    {
        private IDataConverter formatConverter = null;
        public void Publish(List<Product> productList)
        {
            if (formatConverter == null)
            {
                formatConverter = new XmlConverter();
            }
            formatConverter.convert(productList);
        }
    }
    #endregion
}
