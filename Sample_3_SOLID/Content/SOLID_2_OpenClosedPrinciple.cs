﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample_3_SOLID.Content
{
    public class OpenClosedPrinciple
    {
        // Açık-Kapalı Prensibi (OCP : Open Closed Principle)
        // Slogan : "Yazılım varlıkları (classlar, modüller, fonksiyonlar vs.) gelişime AÇIK, kod değişimine KAPALI olmalıdır".
        // Not : Bu prensibi modüle göre  Abstract Class veya Interface ile çözebiliriz.
        // Seneryo : Bir e-ticaret uygulamasında, müşterinin üyelik tipine göre indirim yapmak istiyorsunuz. 
        // Buna göre sistemde, premium ve standart olmak üzere iki tür üyelik tipi var. Gold üyelik tipi geldiğinde ne yapmanız gerekiyor.
    }

    #region Üye indirim örneği
    public abstract class PaymentType
    {
        public abstract decimal Calculate(decimal price);
    }

    public class StandartMemberType : PaymentType
    {
        public override decimal Calculate(decimal price)
        {
            return price;
        }
    }

    public class PremiumMemberType : PaymentType
    {
        public override decimal Calculate(decimal price)
        {
            return price * 1.01M;
        }
    }

    public class GoldMemberType : PaymentType
    {
        public override decimal Calculate(decimal price)
        {
            return price * 1.05M;
        }
    }

    public class Member
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public PaymentType PaymentType { get; set; }
    }

    public class OrderOperations
    {
        public decimal OrderConfirmation(decimal price, Member member)
        {
            return member.PaymentType.Calculate(price);
        }
    }
    #endregion
}
