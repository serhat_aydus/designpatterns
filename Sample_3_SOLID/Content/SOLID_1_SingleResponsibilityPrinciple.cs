﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample_3_SOLID.Content
{
    /// <summary>
    /// Tek Sorumluluk Prensibi (SRP: Single Responsibility Principle)
    /// </summary>
    public class SingleResponsibilityPrinciple
    {
        //  Slogan : “Bir nesneyi değiştirmek için tek bir neden olmalıdır”
        //  Senaryo : Ton balıklı salata hazırlanması istenmiş. 
        //  Uzun bir tezgah üzerinde yan yana çalışan insanlar var. 
        //  İlk sıradaki, domatesleri doğruyor ardından diğeri salatalığı dilimliyor. Üçüncü kişi yeşillikleri yıkayıp güzelce diziyor.
        //  Dördüncü kişi turşuyu hazırlıyor. Beşinci kişi ton balığını salataya ekliyor.
        //  Altıncısı ise hepsini karıştırıp tamamlanan salatayı garsona iletiyor.
        //  Soru = Domateslerin doğranma şeklini değiştirmek istediğinizde bunu kaç kişiye söyleyeceksiniz ?

    }

    #region Pizza Örneği
    public class Pizza
    {
        public string Ad { get; set; }

        public string Boyut { get; set; }

        public double Fiyat { get; set; }


    }

    public class PizzaIslemleri
    {
        public void Pisir(int sure)
        {
            //Pişirme işlemleri...
        }
        public void HamuruAc(int boyut)
        {
            //Hamuru açmak için gereken kodlar...
        }

        public MalzemeIslemleri Malzemeler { get; set; }


    }

    public class MalzemeIslemleri
    {
        public void MalzemeEkle(string malzeme)
        {
            //malzeme ekleme işlemleri
        }

        public void MalzemeyiCikar(string malzeme)
        {
            //listeden malzeme çıkarmak için yapılan işlemler
        }

        public List<string> MalzemeleriGoster()
        {
            //malzeme göstermek için gereken işlemler
            return new List<string>();
        }
    }
    #endregion
}
