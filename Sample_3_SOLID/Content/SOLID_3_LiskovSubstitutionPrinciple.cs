﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample_3_SOLID.Content
{
    public class LiskovSubstitutionPrinciple
    {
        // Liskov Yerdeğiştirme Prensibi (LSP: Liskov Substitution Principle)
        // Slogan : 1- "Temel (base) sınıfın işaretçisini (pointer) ya da referansını kullanan fonksiyonlar, bu sınıftan türemiş olan (derived) 
        //              sınıfları da ekstra bilgiye ihtiyaç duymaksızın kullanabilmelidir".
        //          2- "Aynı temel sınıftan türeyen tüm sınıflar, birbirlerinin yerine kullanılabilir olmalıdır.
        //              Bu yer değiştirme durumunda, sınıfa özel bir istisna kesinlikle oluşmamalıdır".
        // Seneryo : Diyelim ki, farklı veri kaynakları (SQL, Excel dosyası vs.) ile çalışabilecek bir uygulama geliştiriyorsunuz. 
        //           Tüm bu veri kaynakları ile “yükleme” ve “kayıt” işlemleri yapabileceğinizi analiz ettiniz ve bu metotları bir abstract sınıfta (ya da interface) tutmaya karar verdiniz.
        //           Projenizi geliştirirken; XML kaynağına da ihtiyaç duydunuz ve yine aynı abstract sınıftan türeyen XMLSource sınıfını oluşturdunuz. 
        //           Ancak, müşterinin size özellikle vurguladığı bir şey var: “XML dosyaları sadece yüklenebilmeli. Kaydedilmelerini istemiyoruz.
        // Sonuç :   Gerekmedikçe nesneler fazladan metot property vs almaması gerekiyor. Serçeye klima butonu koyup işlevi olmamasının bir anlamı yok.

        #region Data Yükleme ve Kaydetme Örneği
        public interface IRecordable
        {
            void Save();
        }

        public interface IDataSource
        {
            void GetAllData();
        }

        public class DatabaseSource : IDataSource, IRecordable
        {
            public void GetAllData()
            {
                throw new NotImplementedException();
            }

            public void Save()
            {
                throw new NotImplementedException();
            }
        }

        public class XmlSource : IDataSource
        {
            public void GetAllData()
            {
                throw new NotImplementedException();
            }
        }

        public class DataSourceProcess
        {
            public void SaveAll(List<IRecordable> sourceList)
            {
                sourceList.ForEach(s => s.Save());
            }
        }
        #endregion
    }
}
