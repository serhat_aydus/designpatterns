﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample_3_SOLID
{
    class Program
    {
        static void Main(string[] args)
        {
            // Solid Principles
            // S - Single Responsibility Principle (SRP): Nesnenin sadece bir sorumluluğu olmalıdır, yani olası bir değişiklikte tek bir nedene dayandırılmalıdır.
            // O - Open / Closed Principle(OCP): Nesne genişlemeye açık ancak değişikliklere kapalı olmalıdır.
            // L - Liskov 's Substitution Principle (LSP): Programdaki nesnelerin, programın çalışmasında sorun yaratmadan kendi alt örnekleri ile değiştirilebilir olmasıdır. Bu aynı zamanda sözleşmeyle tasarım (Design by Contract ) olarak bilinir.
            // I - Interface Segregation Principle (ISP): Nesnelerin ihtiyaç duymadıkları metodların Interface’lerinden münkün olduğunca ayrıştırılmasıdır. 
            // D - Dependency Inversion Principle (DIP): Yüksek seviyeli sınıflar, düşük seviyeli sınıflara bağlı olmamalı, her ikisi de soyut kavramlara bağlı olmalıdır.

            // Diğer prensibler Araştır sayfa 61
            // KISS Prensibi
            // YAGNI Prensibi
            // DRY Prensibi
            // Reuse Release Equivalency Prensibi
            // Common Closure Prensibi

            // Enterprise tasarım prensipleri Araştır
            // Loose Coupling : Gevşek Bağlantı İlkesi
        }
    }
}
