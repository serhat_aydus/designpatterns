﻿using DesignPatterns.Creational.AbstractFactoryDesignPattern.AbstractProduct;
using DesignPatterns.Creational.AbstractFactoryDesignPattern.ConcreteFactory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Creational.AbstractFactoryDesignPattern.AbstractFactory
{
    public class MSSQLFactory : DatabaseFactory
    {
        public override IConnection CreateConnection()
        {
            return (new MSSQLConnection());
        }

        public override ICommand CreateCommand()
        {
            return (new MSSQLCommand());
        }
    }
}
