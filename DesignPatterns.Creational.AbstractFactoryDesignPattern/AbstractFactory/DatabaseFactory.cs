﻿using DesignPatterns.Creational.AbstractFactoryDesignPattern.AbstractProduct;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Creational.AbstractFactoryDesignPattern.AbstractFactory
{
    public abstract class DatabaseFactory
    {
        public abstract IConnection CreateConnection();
        public abstract ICommand CreateCommand();
    }
}
