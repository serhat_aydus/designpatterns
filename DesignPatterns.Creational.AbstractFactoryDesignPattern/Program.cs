﻿using DesignPatterns.Creational.AbstractFactoryDesignPattern.AbstractFactory;
using DesignPatterns.Creational.AbstractFactoryDesignPattern.ConcreteProduct;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Creational.AbstractFactoryDesignPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            DatabaseOperationManager msSql = new DatabaseOperationManager((new MSSQLFactory()));
            msSql.Connect();
            msSql.Execute();
            msSql.Disconnect();

            Console.WriteLine();

            DatabaseOperationManager oracle = new DatabaseOperationManager((new OracleFactory()));
            oracle.Connect();
            oracle.Execute();
            oracle.Disconnect();

            Console.ReadLine();
        }
    }
}
