﻿using DesignPatterns.Creational.AbstractFactoryDesignPattern.AbstractProduct;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Creational.AbstractFactoryDesignPattern.ConcreteFactory
{
    public class OracleConnection : IConnection
    {
        public void Connect()
        {
            Console.WriteLine("Oracle baglantisi kuruldu.");
        }

        public void Disconnect()
        {
            Console.WriteLine("Oracle baglantisi sonlandırıldı.");
        }
    }
}
