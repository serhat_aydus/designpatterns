﻿using DesignPatterns.Creational.AbstractFactoryDesignPattern.AbstractProduct;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Creational.AbstractFactoryDesignPattern.ConcreteFactory
{
    public class MSSQLConnection : IConnection
    {
        public void Connect()
        {
            Console.WriteLine("MSSQL baglantisi kuruldu.");
        }

        public void Disconnect()
        {
            Console.WriteLine("MSSQL baglantisi sonlandırıldı.");
        }
    }
}
