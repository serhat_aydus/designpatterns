﻿using DesignPatterns.Creational.AbstractFactoryDesignPattern.AbstractProduct;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Creational.AbstractFactoryDesignPattern.ConcreteFactory
{
    class OracleCommand : ICommand
    {
        public void Execute()
        {
            Console.WriteLine("Oracle sorgusu calistiriliyor.");
        }
    }
}
