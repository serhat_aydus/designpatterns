﻿using DesignPatterns.Creational.AbstractFactoryDesignPattern.AbstractProduct;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Creational.AbstractFactoryDesignPattern.ConcreteFactory
{
    public class MSSQLCommand : ICommand
    {
        public void Execute()
        {
            Console.WriteLine("MSSQL sorgusu calistiriliyor.");
        }
    }
}
