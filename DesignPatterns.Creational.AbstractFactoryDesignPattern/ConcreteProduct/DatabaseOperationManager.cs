﻿using DesignPatterns.Creational.AbstractFactoryDesignPattern.AbstractFactory;
using DesignPatterns.Creational.AbstractFactoryDesignPattern.AbstractProduct;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Creational.AbstractFactoryDesignPattern.ConcreteProduct
{
  public  class DatabaseOperationManager
    {
        private DatabaseFactory _databaseFactory;
        private IConnection _connection;
        private ICommand _command;

        public DatabaseOperationManager(DatabaseFactory databaseFactory)
        {
            _databaseFactory = databaseFactory;
            _connection = databaseFactory.CreateConnection();
            _command = databaseFactory.CreateCommand();
        }

        public void Connect()
        {
            _connection.Connect();
        }

        public void Disconnect()
        {
            _connection.Disconnect();
        }

        public void Execute()
        {
            _command.Execute();
        }
    }
}
