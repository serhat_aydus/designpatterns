﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Creational.AbstractFactoryDesignPattern.AbstractProduct
{
    public interface IConnection
    {
        void Connect();
        void Disconnect();
    }
}
