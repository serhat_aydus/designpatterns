﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Behavioral.TemplateMethodDesignPattern.AbstractClass
{
  public abstract  class AkisBaslatma
    {
        public abstract void AkisBilgileriOlustur();
        public abstract void AkisBaslat();
        public void TemplateMethod()
        {
            AkisBilgileriOlustur();
            AkisBaslat();
        }
    }
}
