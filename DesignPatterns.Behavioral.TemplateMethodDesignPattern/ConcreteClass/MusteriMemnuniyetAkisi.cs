﻿using DesignPatterns.Behavioral.TemplateMethodDesignPattern.AbstractClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Behavioral.TemplateMethodDesignPattern.ConcreteClass
{
    public class MusteriMemnuniyetAkisi : AkisBaslatma
    {
        public string MusteriAdı { get { return "ByCoder"; } }

        public override void AkisBilgileriOlustur()
        {
            Console.WriteLine("Memnuniyet Akışı - Müşteri Adı : " + MusteriAdı);
        }

        public override void AkisBaslat()
        {
            Console.WriteLine("Müşteri Memnuniyeti Akışı Başlatıldı ");
        }
    }
}
