﻿using DesignPatterns.Behavioral.TemplateMethodDesignPattern.AbstractClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Behavioral.TemplateMethodDesignPattern.ConcreteClass
{
    public class CalisanAkisi : AkisBaslatma
    {
        public string CalisanAdı { get { return "ByCoder"; } }

        public override void AkisBilgileriOlustur()
        {
            Console.WriteLine("Çalışan Akışı - Çalışan Adı : " + CalisanAdı);
        }

        public override void AkisBaslat()
        {
            Console.WriteLine(CalisanAdı + "İsimli çalışan için internet yetkisi istendi");
        }
    }
}
