﻿using DesignPatterns.Creational.FactoryMethodDesignPattern.Creator;
using DesignPatterns.Creational.FactoryMethodDesignPattern.Enums;
using DesignPatterns.Creational.FactoryMethodDesignPattern.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Creational.FactoryMethodDesignPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            // 1. Kullanım Şekli
            ProductCreator c = new ProductCreator();
            IProduct product1 = c.FactoryMethod(ProductType.Product1);
            Console.WriteLine(product1.ShipFrom());
            IProduct product2 = c.FactoryMethod(ProductType.Product2);
            Console.WriteLine(product2.ShipFrom());
            IProduct product3 = c.FactoryMethod(ProductType.Product3);
            Console.WriteLine(product3.ShipFrom());
            Console.ReadKey();
        }
    }
}
