﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Creational.FactoryMethodDesignPattern.Enums
{
    public enum ProductType
    {
        Product1,
        Product2,
        Product3,
    }
}
