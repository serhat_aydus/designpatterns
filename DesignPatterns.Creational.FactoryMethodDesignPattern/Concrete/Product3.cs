﻿using DesignPatterns.Creational.FactoryMethodDesignPattern.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Creational.FactoryMethodDesignPattern.Classess
{
    public class Product3 : IProduct
    {
        public String ShipFrom()
        {
            return "Amerika'dan gelen ürünler.";
        }
    }
}
