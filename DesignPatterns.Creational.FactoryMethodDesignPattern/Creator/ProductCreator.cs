﻿using DesignPatterns.Creational.FactoryMethodDesignPattern.Classess;
using DesignPatterns.Creational.FactoryMethodDesignPattern.Enums;
using DesignPatterns.Creational.FactoryMethodDesignPattern.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Creational.FactoryMethodDesignPattern.Creator
{
    public class ProductCreator
    {
        public IProduct FactoryMethod(ProductType productType)
        {
            IProduct product = null;
            switch (productType)
            {
                case ProductType.Product1:
                    product = new Product1();
                    break;
                case ProductType.Product2:
                    product = new Product2();
                    break;
                case ProductType.Product3:
                    product = new Product3();
                    break;
            }
            return product;
        }
    }
}
