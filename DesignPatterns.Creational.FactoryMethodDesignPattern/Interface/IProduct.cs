﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Creational.FactoryMethodDesignPattern.Interface
{
    public interface IProduct
    {
        string ShipFrom();
    }
}
