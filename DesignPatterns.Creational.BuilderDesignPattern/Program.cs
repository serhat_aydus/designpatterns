﻿using DesignPatterns.Creational.BuilderDesignPattern.Builder;
using DesignPatterns.Creational.BuilderDesignPattern.ConcereteBuilder;
using DesignPatterns.Creational.BuilderDesignPattern.Director;
using DesignPatterns.Creational.BuilderDesignPattern.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Creational.BuilderDesignPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            ProductCar productCar = new ProductCar();

            CarBuilder fordBuilder = new FordBuilder();
            productCar.CreateCar(fordBuilder);
            DisplayCarProperties(fordBuilder.Car);

            CarBuilder audiBuilder = new AudiBuilder();
            productCar.CreateCar(audiBuilder);
            DisplayCarProperties(audiBuilder.Car);

            CarBuilder volkswagenBuilder = new VolkswagenBuilder();
            productCar.CreateCar(volkswagenBuilder);
            DisplayCarProperties(volkswagenBuilder.Car);

            Console.ReadKey();
        }

        public static void DisplayCarProperties(Car car)
        {
            Console.WriteLine("Marka: {0} | Model: {1} | Motor: {2} ", car.Brand, car.Model, car.Engine);
        }
    }
}
