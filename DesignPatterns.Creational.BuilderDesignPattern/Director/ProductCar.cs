﻿using DesignPatterns.Creational.BuilderDesignPattern.Builder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Creational.BuilderDesignPattern.Director
{
    /// <summary>
    /// Director
    /// </summary>
    public class ProductCar
    {
        #region Methods
        public void CreateCar(CarBuilder carBuilder)
        {
            carBuilder.SetBrand();
            carBuilder.SetModel();
            carBuilder.SetEngine();
        }
        #endregion
    }
}
