﻿namespace DesignPatterns.Creational.BuilderDesignPattern.Product
{
    /// <summary>
    /// Product Sınıfı
    /// </summary>
    public class Car
    {
        #region Properties
        public string Brand { get; set; }
        public string Model { get; set; }
        public string Engine { get; set; }
        #endregion
    }
}
