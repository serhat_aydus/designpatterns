﻿using DesignPatterns.Creational.BuilderDesignPattern.Builder;
using DesignPatterns.Creational.BuilderDesignPattern.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Creational.BuilderDesignPattern.ConcereteBuilder
{
    /// <summary>
    /// Concrete Builder
    /// </summary>
    public class FordBuilder : CarBuilder
    {
        #region Constructor
        public FordBuilder()
        {
            car = new Car();
        }
        #endregion

        #region Methods
        public override void SetBrand()
        {
            car.Brand = "Ford";
        }

        public override void SetEngine()
        {
            car.Engine = "1.6 Motor";
        }

        public override void SetModel()
        {
            car.Model = "Focus";
        }
        #endregion
    }
}
