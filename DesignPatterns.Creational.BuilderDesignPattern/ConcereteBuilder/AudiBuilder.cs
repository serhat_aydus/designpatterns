﻿using DesignPatterns.Creational.BuilderDesignPattern.Builder;
using DesignPatterns.Creational.BuilderDesignPattern.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Creational.BuilderDesignPattern.ConcereteBuilder
{
    /// <summary>
    /// Concrete Builder
    /// </summary>
    public class AudiBuilder : CarBuilder
    {
        #region Constructor
        public AudiBuilder()
        {
            car = new Car();
        }
        #endregion

        #region Methods
        public override void SetBrand()
        {
            car.Brand = "Audi";
        }

        public override void SetEngine()
        {
            car.Engine = "1.8 Motor";
        }

        public override void SetModel()
        {
            car.Model = "A3";
        }
        #endregion
    }
}
