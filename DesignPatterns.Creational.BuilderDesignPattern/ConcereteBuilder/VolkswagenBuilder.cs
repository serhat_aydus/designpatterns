﻿using DesignPatterns.Creational.BuilderDesignPattern.Builder;
using DesignPatterns.Creational.BuilderDesignPattern.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Creational.BuilderDesignPattern.ConcereteBuilder
{
    /// <summary>
    /// Concrete Builder
    /// </summary>
    public class VolkswagenBuilder : CarBuilder
    {
        #region Constructor
        public VolkswagenBuilder()
        {
            car = new Car();
        }
        #endregion

        #region Methods
        public override void SetBrand()
        {
            car.Brand = "Volkswagen";
        }

        public override void SetEngine()
        {
            car.Engine = "1.4 Motor";
        }

        public override void SetModel()
        {
            car.Model = "Polo";
        }
        #endregion
    }
}
