﻿using DesignPatterns.Creational.BuilderDesignPattern.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Creational.BuilderDesignPattern.Builder
{
    /// <summary>
    /// Builder
    /// </summary>
    public abstract class CarBuilder
    {
        #region Field
        protected Car car;
        #endregion

        #region Properties
        public Car Car
        {
            get
            {
                return car;
            }
        }
        #endregion

        #region Methods
        public abstract void SetBrand();
        public abstract void SetModel();
        public abstract void SetEngine();
        #endregion
    }
}
