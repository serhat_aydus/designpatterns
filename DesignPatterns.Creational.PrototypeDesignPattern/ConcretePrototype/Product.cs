﻿using DesignPatterns.Creational.PrototypeDesignPattern.Prototype;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Creational.PrototypeDesignPattern.ConcretePrototype
{
    public class Product : IPrototype
    {
        #region Field
        private Guid _id;
        #endregion

        #region Properties
        public string Name { get; set; }
        public decimal Price { get; set; }
        public Guid Id
        {
            get
            {
                return _id;
            }
        }
        #endregion


        #region Constructor
        public Product()
        {
            _id = Guid.NewGuid();
        }
        #endregion

        #region Methods
        public object Clone()
        {
            return this.MemberwiseClone() as IPrototype;
        }
        #endregion

    }
}
