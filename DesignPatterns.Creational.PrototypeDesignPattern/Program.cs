﻿using DesignPatterns.Creational.PrototypeDesignPattern.ConcretePrototype;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Creational.PrototypeDesignPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            Product product1 = new Product();
            product1.Name = "Ürün 1";
            product1.Price = 550;

            Product product2 = product1.Clone() as Product;
            product1.Name = "Ürün 2";

            Product product3 = product1.Clone() as Product;
            product3.Name = "Ürün 3";
            product3.Price = 800;

            // Id değerini yazdırma nedenimiz test için olup bu 3 objeninde Id değeri aynı olmalıdır.
            Console.WriteLine(string.Format("Test ID : {0} / Ürün : {1} => {2} TL", product1.Id, product1.Name, product1.Price));
            Console.WriteLine(string.Format("Test ID : {0} / Ürün : {1} => {2} TL", product2.Id, product2.Name, product2.Price));
            Console.WriteLine(string.Format("Test ID : {0} / Ürün : {1} => {2} TL", product3.Id, product3.Name, product3.Price));

            Console.ReadKey();
        }
    }
}
