﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Creational.SingletonDesignPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            Product product1 = Product.GetInstance();
            Product product2 = Product.GetInstance();
            Product product3 = Product.GetInstance();
            Product product4 = Product.GetInstance();

            // Id değerini yazdırma nedenimiz test için olup bu 4 objeninde Id değeri aynı olmalıdır.
            Console.WriteLine("1.Nesne Guid Değeri : " + product1.Id);
            Console.WriteLine("2.Nesne Guid Değeri : " + product2.Id);
            Console.WriteLine("3.Nesne Guid Değeri : " + product3.Id);
            Console.WriteLine("4.Nesne Guid Değeri : " + product4.Id);
            Console.ReadLine();
        }
    }
}
