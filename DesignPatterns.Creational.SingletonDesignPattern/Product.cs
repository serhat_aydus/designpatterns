﻿using System;

namespace DesignPatterns.Creational.SingletonDesignPattern
{
    /// <summary>
    /// Singleton Design Pattern
    /// </summary>
    public class Product
    {
        #region Field
        private static Product _instance;
        private static object _lockObject = new object();
        private Guid _id;
        #endregion

        #region Properties
        public Guid Id
        {
            get
            {
                return _id;
            }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Instance oluşturulmaması için private yapılmıştır.
        /// </summary>
        private Product(Guid id)
        {
            _id = id;
        }
        #endregion

        #region Methods
        public static Product GetInstance()
        {
            if (_instance == null)
            {
                lock (_lockObject)
                {
                    if (_instance == null)
                    {
                        _instance = new Product(Guid.NewGuid());
                    }
                }

            }
            return _instance;
        }
        #endregion
    }
}
