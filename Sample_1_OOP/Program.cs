﻿using Sample_1_OOP.Contents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample_1_OOP
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Encapsulation
            //Human human = new Human();
            //human.Name = "Ali";
            //human.Surname = "KOŞAR";
            //human.BirthDate = DateTime.Parse("1985-05-23");
            //Console.WriteLine(string.Format("Name = {0} {1} | Age = {2}", human.Name, human.Surname, human.Age));
            #endregion

            #region Inheritance
            //Programmer programmer = new Programmer();
            //programmer.Name = "Ahmet";
            //programmer.Surname = "GİDER";
            //programmer.Gender = Gender.Male;
            //programmer.Salary = 2000;
            //programmer.CheckForError();
            //programmer.Build();
            //Console.WriteLine(string.Format("Salary = {0} $", programmer.SalaryAmount()));

            //Tester tester = new Tester();
            //tester.Name = "Ayşe";
            //tester.Surname = "KOŞAR";
            //tester.Gender = Gender.Woman;
            //tester.Salary = 1500;
            //tester.CheckForError();
            //tester.Test();
            //Console.WriteLine(string.Format("Salary = {0} $", tester.SalaryAmount()));
            #endregion

            #region Abstract Class
            WebDepartment web = new WebDepartment();
            web.AuthorizedPerson = "Ayşe GELMEZ";
            web.WorkingHours();

            SupportDepartment support = new SupportDepartment();
            support.AuthorizedPerson = "Cem KOŞAR";
            support.WorkingHours();
            #endregion

            Console.ReadLine();
        }
    }
}
