﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample_1_OOP.Contents
{
    /// <summary>
    /// Class
    /// </summary>
    class Sample
    {
        // Field
        string field = string.Empty;
        // Property
        string property { get; set; }
        // Method
        string GetAge()
        {
            return "Result";
        }
    }
}