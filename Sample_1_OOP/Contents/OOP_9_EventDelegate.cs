﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample_1_OOP.Contents
{
    public class ProductEventArgs : EventArgs
    {
        public string ProductName { get; set; }
        public ProductEventArgs(string productName)
        {
            this.ProductName = productName;
        }
    }

    public class ProductOperations
    {
        public delegate void ProductInsertEventHandler(ProductEventArgs e);
        public event ProductInsertEventHandler productInsertEvent;

        private void ProductInsert(string productName)
        {
            ProductEventArgs e = new ProductEventArgs("Laptop");
            onAddedProducts(e);
        }

        private void onAddedProducts(ProductEventArgs e)
        {
            if (productInsertEvent != null)
            {
                ProductInsert(e.ProductName);
            }
        }
    }
}
