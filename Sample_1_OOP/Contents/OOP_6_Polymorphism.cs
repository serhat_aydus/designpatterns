﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample_1_OOP.Contents
{
    public class Staff : Human
    {
        public double Salary { get; set; }

        public virtual double SalaryAmount()
        {
            return Salary * 2;
        }
    }

    public partial class Programmer
    {
        public override double SalaryAmount()
        {
            return this.Salary * 3;
        }
    }

    public partial class Tester
    {

    }
}
