﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample_1_OOP.Contents
{
    public partial class Programmer : Staff, IStaff, IProgrammer
    {
        public void CheckForError()
        {
            Console.WriteLine(string.Format("{0} {1} {2} test OK.", this.Gender, this.Name, this.Surname));
        }

        public void Build()
        {
            Console.WriteLine(string.Format("{0} {1} {2} program has successfully build", this.Gender, this.Name, this.Surname));
        }
    }

    public partial class Tester : Staff, IStaff, ITester
    {
        public void CheckForError()
        {
            Console.WriteLine(string.Format("{0} {1} {2} test OK.", this.Gender, this.Name, this.Surname));
        }

        public void Test()
        {
            Console.WriteLine(string.Format("{0} {1} {2} program has successfully tested", this.Gender, this.Name, this.Surname));
        }
    }
}
