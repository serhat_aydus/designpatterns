﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample_1_OOP.Contents
{
    public abstract class Department
    {
        public abstract string Name { get; }
        public string AuthorizedPerson { get; set; }
        public abstract void WorkingHours();
    }

    public class WebDepartment : Department
    {
        public override string Name
        {
            get
            {
                return "Web Department";
            }
        }

        public override void WorkingHours()
        {
            Console.WriteLine(string.Format("Departman = {0} AuthorizedPerson = {1}  WorkingHours = 09-00 : 18-00", Name, this.AuthorizedPerson));
        }
    }

    public class SupportDepartment : Department
    {
        public override string Name
        {
            get
            {
                return "Support Department";
            }
        }

        public override void WorkingHours()
        {
            Console.WriteLine(string.Format("Departman = {0} AuthorizedPerson = {1}  WorkingHours = 08-00 : 17-00", Name, this.AuthorizedPerson));
        }
    }

}
