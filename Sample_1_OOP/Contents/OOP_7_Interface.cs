﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample_1_OOP.Contents
{
    public interface IStaff
    {
        void CheckForError();
    }

    public interface IProgrammer
    {
        void Build();
    }

    public interface ITester
    {
        void Test();
    }
}
