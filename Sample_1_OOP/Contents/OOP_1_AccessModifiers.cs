﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample_1_OOP.Contents
{
    class AccessModifiers
    {
        #region private
        private string Name = string.Empty;
        /*
            * En temen erişim düzenleyicisidir.
            * Yalnızca o tipin içerisinden erişilmesine izin verir ve o tipe “özel” olmasını sağlar.
            * Private üyeler miras yoluyla türetilmiş sınıflara aktarılamazlar.
            * Bir üyenin önüne hiçbir erişim düzenleyici anahtar kelimesi belirtmezseniz, o üye derleyici tarafından private olarak algılanır.
            * Varsayılan private erişim belirleyicisidir.
        */
        #endregion

        #region public
        public string Surname = string.Empty;
        /*
             * En genel ve sınırsız erişim düzenleyicisidir.
             * Hem tip için hem de, tip üyeleri için kullanılabilir. 
             * Bir sınıfın public ise bu, o sınıfın, bulunduğu assembly dışından(referans olarak alındığı başka bir assembly’den) her türlü erişilebilir.
             * Peki, tipin üyeleri public ise o üyeye her yerden erişilebilir.
             * Miras yolu ile türetilmiş sınıflara aktarılırlar.
        */
        #endregion

        #region protected
        protected string Age = string.Empty;
        /*
            * Bu üyeye yalnızca ait olduğu tip içinden ulaşılabilir. 
            * Private’den farkı nedir? İşte tek fark: protected olarak tanımlanmış alan ya da metodlar, miras olarak aktarılabiliriler.
            * Yani, “bu üyeye kesinlikle dışardan ulaşılamasın, ama miras bırakılabilsin” diyorsanız, o üye protected olmalı.
            * Böylece, kabaca “miras bırakılabilen, fakat dışarıdan ulaşılamayan alanlar protected erişim düzenleyicisi ile belirlenir.
         */
        #endregion

        #region internal
        internal string BirthDate = string.Empty;
        /*
            * “Dahili” anlamına gelmektedir.Yalnızca bulunduğu assembly’den erişilebilir. 
            * Burada assembly ifadesinden kasıt, projenin kendisidir. 
            * Yani, bir kütüphane (.dll) oluşturuyorsanız, internal bir sınıfa sadece o kütüphaneden ulaşabilirsiniz.Bu erişim düzenleyicisi, sınıf üyelerinde de kullanılabilir. 
            * Onda da etkisi aynıdır. Bir sınıfın erişim düzenleyicisi belirtilmezse, varsayılan olarak internal kabul edilir.
         */
        #endregion

        #region protected internal
        protected internal string gender = string.Empty;
        /*
            * Yalnızca sınıf üyelerine uygulanır.
            * Erişim yapılan yere göre “internal” ya da “protected” davranır.
            * Doğal olarak assembly dışından erişilmeye çalışıldığında internal, aynı assembly’den erişilmeye çalışıldığında ise protected davranır.
        */
        #endregion
    }
}
