﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample_1_OOP.Contents
{
    public class Human
    {
        string _name;
        public string Name
        {
            get
            {
                return _name;
            }

            set
            {
                _name = value;
            }
        }

        string _surname;
        public string Surname
        {
            get
            {
                return _surname;
            }

            set
            {
                _surname = value;
            }
        }

        /// <summary>
        /// read-only property
        /// </summary>
        int _age;
        public int Age
        {
            get
            {
                _age = DateTime.Now.Year - _birthDate.Year;
                return _age;
            }
        }

        /// <summary>
        /// write-only
        /// </summary>
        DateTime _birthDate;
        public DateTime BirthDate
        {
            set
            {
                _birthDate = value;
            }
        }

        Gender _gender;
        public Gender Gender
        {
            get
            {
                return _gender;
            }

            set
            {
                _gender = value;
            }
        }
    }
}
